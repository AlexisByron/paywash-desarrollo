<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('itr_model');
        $this->load->model('usuarios_model');
    }

    public function login() {
        $this->load->view('login_view');
    }


    public function index()
	{
		$this->load->view('admin_view');
	}

    public function home(){
        $this->load->view('home');
    }

    public function lavados(){
        $this->load->view('admin_view');
    }

//    public function GetTotales(){
//        $arr=[
//            "concesionario"=>$_SESSION['user']['concesionario'],
//            "tipo_reporte"=>"anual"
//        ];
//        $payload = json_encode($arr);
//
//        $ch = curl_init('https://paywash.tecnoandina.cl/api/paywash/concesionario/reporte/count');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//
//        // Set HTTP Header for POST request
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'header: x-access-token :'.$_SESSION['user']['token'],
//                'Content-Length: ' . strlen($payload))
//        );
//
//        // Submit the POST request
//        $result = curl_exec($ch);
//
//
//        $datos=json_decode($result,true);
//
//
//        //echo $result;
//        $recaudado = number_format($datos["data"]["recaudado"], 0, ',', '.');
//        $arr=[
//            "total_lavadoras"=>$datos["data"]["total_lavadoras"],
//            "total_secadoras"=>$datos["data"]["total_secadoras"],
//            "recaudado"=>$recaudado,
//            "total_lavado"=>$datos["data"]["total_lavado"],
//            "fallas"=>$datos["data"]["fallas"]
//        ];
//
//
//        //$this->session->set_userdata("total",$datos["data"]["total_lavado"]);
//
//
//        //Close cURL session handle
//
//        echo json_encode($arr);
//        curl_close($ch);
//
//
//    }

//    public function graficoFiltro(){
//        $arrGrafico=array();
//        $filtro='week';
//        if($_POST!=null){
//            $filtro=$_POST["filtro"];
//            if(array_key_exists("fecha_inicio", $_POST)){
//                $fecha_inicio=gmdate($_POST['fecha_inicio']);
//                $fecha_termino=gmdate($_POST['fecha_termino']);
//            }
//
//        }
//        switch ($filtro){
//            case "week":
//                    $arr=[
//                        "concesionario"=>$_SESSION['user']['concesionario'],
//                        "tipo_reporte"=>"semanal"
//                    ];
//                break;
//            case "month":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"mensual"
//                ];
//                break;
//            case "year":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"anual"
//                ];
//                break;
//            case "calendario":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"semanal",
//                    "desde"=>$fecha_inicio,
//                    "hasta"=>$fecha_termino,
//                ];
//                break;
//
//
//        }
//        $payload = json_encode($arr);
//
//        $ch = curl_init('https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte/count');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
////        header: x-access-token =$_SESSION['user']['token'];
//        // Set HTTP Header for POST request
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'header: x-access-token :'.$_SESSION['user']['token'],
//                'Content-Length: ' . strlen($payload))
//        );
//
//        // Submit the POST request
//        $result = curl_exec($ch);
//
//
//
//        $datos=json_decode($result,true);
//
//
//
//        foreach ($datos["data"]["total"] as $key => $val) {
//
//            $time[$key] = $val["fecha"];
//        }
//
//
//        array_multisort($time, SORT_ASC, $datos["data"]["total"]);
//
//        $this->session->set_userdata("total",$datos["data"]["total_lavado"]);
//
//        foreach ($datos["data"]["total"] as $key){
//            $recaudado = number_format($datos["data"]["recaudado"], 0, ',', '.');
//            $dato = [
//                $key["fecha"] ,
//                $key["cantidad"],
//
//
//                "session" => $datos["data"]["total_lavado"],
//                "total_lavadoras"=>$datos["data"]["total_lavadoras"],
//                "total_secadoras"=>$datos["data"]["total_secadoras"],
//                "recaudado"=>$recaudado ,
//                "fallas"=>$datos["data"]["fallas"]
//            ];
//            array_push($arrGrafico,$dato);
//        }
//
//        // Close cURL session handle
//        //$data['data']=$arrGrafico;
//        echo json_encode($arrGrafico);
//        curl_close($ch);
//
//        exit;
//    }

//    function date_compare($a, $b)
//    {
//        $t1 = strtotime($a['datetime']);
//        $t2 = strtotime($b['datetime']);
//        return $t1 - $t2;
//    }

//    public  function datosLavadosComuna(){
//
//        $arrComuna=array();
//        $filtro='year';
//        if($_POST!=null){
//            if(array_key_exists("filtro", $_POST)){
//                $filtro=$_POST['filtro'];
//            }else{
//                $filtro="personalizado" ;
//                $fecha_inicio=gmdate($_POST['fInicio']);
//                $fecha_termino=gmdate($_POST['fTermino']);
//            }
//
//        }
//
//        switch ($filtro){
//            case "week":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"semanal"
//                ];
//                break;
//            case "month":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"mensual"
//                ];
//                break;
//            case "year":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"anual"
//                ];
//                break;
//            case "personalizado":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"semanal",
//                    "desde"=>$fecha_inicio,
//                    "hasta"=>$fecha_termino,
//                ];
//                break;
//        }
//    $payload = json_encode($arr);
//
//
//    $ch = curl_init('https://paywash.tecnoandina.cl/api/paywash/concesionario/reporte/count');
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//
//    // Set HTTP Header for POST request
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'header: x-access-token :'.$_SESSION['user']['token'],
//            'Content-Length: ' . strlen($payload))
//    );
//
//    // Submit the POST request
//    $result = curl_exec($ch);
////    var_dump($result);
////    exit;
//
//
//    $datos=json_decode($result,true);
//    $this->session->set_flashdata("total",$datos["data"]["total_lavado"]);
//
//
//    foreach ($datos["data"]["comunas"] as $key){
//        $recaudado = number_format($datos["data"]["recaudado"], 0, ',', '.');
//        $dato = [
//            "concesionario" => $key["nombre"],
//            "cantidad" => $key["cantidad"],
//            "ranking"=>"<img src=\"https://smartkitchen.cl/paywash/assets/imgs/normal.png\" class=\"ranking\" alt=\"no esta\">",
//
//            "session" => $datos["data"]["total_lavado"],
//            "total_lavadoras"=>$datos["data"]["total_lavadoras"],
//            "total_secadoras"=>$datos["data"]["total_secadoras"],
//            "recaudado"=>$recaudado,
//            "fallas"=>$datos["data"]["fallas"]
//        ];
//            $existe=false;
//            if(count($arrComuna)>0){
//                for ($i = 0; $i < count($arrComuna); ++$i) {
//                    //echo $arrComuna[$i]["concesionario"]." ".$key["nombre"]." ".$i."<br>";
//                    if($arrComuna[$i]["concesionario"]==$key["nombre"]){
//                     $existe=true;
//                     $arrComuna[$i]["cantidad"]=$arrComuna[$i]["cantidad"]+$key["cantidad"];
//                    }
//                }
//
//                if(!$existe){
//                    array_push($arrComuna,$dato);
//                }
//            }else{
//                array_push($arrComuna,$dato);
//            }
//
//
//    }
//
//    // Close cURL session handle
//    $data['data']=$arrComuna;
//    echo json_encode($data);
//    curl_close($ch);
//
//    exit;
//}
//
//    public  function datosLavadosEdificio(){
//
//        $arrEdiifcio=array();
//        $filtro='year';
//        if($_POST!=null){
//            if(array_key_exists("filtro", $_POST)){
//                $filtro=$_POST['filtro'];
//            }else if(array_key_exists("fInicio", $_POST) && array_key_exists("fTermino", $_POST)){
//                $filtro="personalizado" ;
//                $fecha_inicio=gmdate($_POST['fInicio']);
//                $fecha_termino=gmdate($_POST['fTermino']);
//            }else{
//                $filtro="week";
//            }
//
//        }
//
//
//        switch ($filtro){
//            case "week":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"semanal"
//                ];
//                break;
//            case "month":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"mensual"
//                ];
//                break;
//            case "year":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"anual"
//                ];
//                break;
//            case "personalizado":
//                $arr=[
//                    "concesionario"=>$_SESSION['user']['concesionario'],
//                    "tipo_reporte"=>"semanal",
//                    "desde"=>$fecha_inicio,
//                    "hasta"=>$fecha_termino,
//                ];
//                break;
//        }
//        $payload = json_encode($arr);
//
//
//        $ch = curl_init('https://paywash.tecnoandina.cl/api/paywash/concesionario/reporte/count');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//
//        // Set HTTP Header for POST request
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'header: x-access-token :'.$_SESSION['user']['token'],
//                'Content-Length: ' . strlen($payload))
//        );
//
//        // Submit the POST request
//        $result = curl_exec($ch);
//
//
//        $datos=json_decode($result,true);
//        //$this->session->set_flashdata("total",$datos["data"]["total_lavado"]);
//        //var_dump($datos);
//
//       foreach ($datos["data"]["edificios"] as $key){
//            $dato = [
//                "concesionario" => $key["nombre"],
//                "cantidad" => $key["cantidad"],
//                "ranking"=>"<img src=\"https://smartkitchen.cl/paywash/assets/imgs/normal.png\" class=\"ranking\" alt=\"no esta\">",
//                "session" => $datos["data"]["total_lavado"]
//            ];
//           $existe=false;
//           if(count($arrEdiifcio)>0){
//               for ($i = 0; $i < count($arrEdiifcio); ++$i) {
//                   //echo $arrComuna[$i]["concesionario"]." ".$key["nombre"]." ".$i."<br>";
//                   if($arrEdiifcio[$i]["concesionario"]==$key["nombre"]){
//                       $existe=true;
//                       $arrEdiifcio[$i]["cantidad"]=$arrEdiifcio[$i]["cantidad"]+$key["cantidad"];
//                   }
//               }
//
//               if(!$existe){
//                   array_push($arrEdiifcio,$dato);
//               }
//           }else{
//               array_push($arrEdiifcio,$dato);
//           }
//        }
//
//        //Close cURL session handle
//        $data['data']=$arrEdiifcio;
//        echo json_encode($data);
//        curl_close($ch);
//
//        exit;
//    }


    public function historial(){
        if($_SESSION['user']['perfil']=='operario'){
            $this->load->view("historial");
        }else{
            redirect('https://smartkitchen.cl/paywash/inicio');
        }

    }


}

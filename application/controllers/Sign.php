<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

    }


    public function index()
    {
        redirect('sign/login');
    }

    public function login()
    {
        $data['the_content'] = $this->load->view('sign/login_view', null, TRUE);
        $this->load->view('sign_view', $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->destroy_user();
        redirect('sign/index');
    }

    public function unlock()
    {
        $this->output->set_content_type('application_json');
        $user = $this->get_user();
        if( $user['login_password'] == $this->input->post('password') )
        {
            $this->output->set_output(json_encode(array('result' => 1)));
            $this->session->set_userdata('bloqueo', 0);
        }
        else
        {
            $this->output->set_output(json_encode(array('result' => 0)));
        }
        return false;
    }

    public function post_login()
    {
//        $this->output->set_content_type('application_json');
//        $credenciales = $this->input->post();
//        $credenciales = array(
//            'usuario' => $_POST["usuario"],
//            'password' =>  $_POST["password"]
//        );
//
//
//        $payload = json_encode($credenciales);
//
//        $ch = curl_init('https://paywash.tecnoandina.cl:1881/api/paywash/login');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//
//        // Set HTTP Header for POST request
//
//
//        // Submit the POST request
//        $result = curl_exec($ch);
//
//         $user = $this->sign_model->valid_user($credenciales);
        $user=$_POST["usuario"];

        if($user) {
//            $this->session->set_userdata(array('id' => $user['id']));

            $usuario=[
                "user"=>$user['nombre'],
                "perfil"=>$user['permiso'],
                "concesionario"=>$user['concesionario'],
                "token"=>$_POST["token"]
            ];

            $_SESSION['user']=$usuario;
           // $this->output->set_output(json_encode(array('result' => 1, 'session' => $this->session->get_userdata())));
            $this->output->set_output(json_encode(array('result'=> 1)));
            return true;
        }
        else
        {
            $this->output->set_output(json_encode(array('result'=> 0)));
            return false;
        }
    }

    public function bloquear_sistema()
    {
        $this->session->set_userdata('bloqueo', 1);
    }


}

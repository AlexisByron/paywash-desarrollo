<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    private $user;
    private $consumer_key;
    private $consumer_secret;
    private $api_options;
    private $woo_client;

    private $abstract_data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sign_model');
        //$this->load->model('api_model');
        //$this->load->model('usuario_model');
        //$this->load->model('notificaciones_model');
        //$this->load->model('compras_model');

        //Autenticación de usuario
        $this->user = $this->getSessionData();

        $no_redirect = array('sign/index', 'sign/login', 'sign/post_login', 'sign/logout');
//        if (!$this->user && !in_array(uri_string(), $no_redirect)) {
//            redirect('sign/index');
//        }

//        //Conexión con API
//        $this->consumer_key = 'ck_eac95fac0c2abfedadbf5e73a86822baaa27fda2';
//        $this->consumer_secret = 'cs_a1b51309ba5030efa4b77e5f05061dc8dcbf668d';
//        $this->options = array(
//            'ssl_verify'	=> false,
//        );


        //Notificaciones
//        $this->abstract_data['notificaciones']['clean_imagenes_tmp'] = $this->notificaciones_model->imagenes_tmp();
//        $this->abstract_data['notificaciones']['clean_cotizaciones'] = $this->notificaciones_model->clean_cotizaciones();
//        $this->abstract_data['qty_notificaciones'] = $this->notificaciones_model->get_qty_notificaciones($this->abstract_data['notificaciones']);

        //Compras registradas
//        $this->abstract_data['compras_por_revisar'] = $this->compras_model->get_where('compras', array('status_revisada' => 0));
    }

    public function get_abstract_data()
    {
        //return $this->abstract_data;
    }

    public function get_user()
    {
        //return $this->user;
    }

    public function destroy_user()
    {
        //$this->user = null;
    }

    public function get_woo_client()
    {
        //return $this->woo_client;
    }

    private function getSessionData()
    {
        $user_id = $this->session->userdata('id');
        return $this->sign_model->getSessionData($user_id);
    }

    public function usuario_envia_cotizacion($order_id, $id_cotizacion_atendida)
    {
        //$this->usuario_model->usuario_envia_cotizacion($order_id, $id_cotizacion_atendida, $this->get_user());
    }

    public function enviar_correo_registro_cliente($datos_email = array())
    {
//        $config = Array(
//            'protocol' => $this->config->item('protocol'),
//            'smtp_host' => $this->config->item('smtp_host'),
//            'smtp_port' => $this->config->item('smtp_port'),
//            'smtp_user' => $this->config->item('smtp_user'),
//            'smtp_pass' => $this->config->item('smtp_pass'),
//            'smtp_timeout' => $this->config->item('smtp_timeout'),
//            'mailtype'  => $this->config->item('mailtype'),
//            'charset'   => $this->config->item('charset'),
//            'wordwrap' => $this->config->item('wordwrap'),
//        );
//        $this->load->library('email', $config);
//        $this->email->set_newline("\r\n");
//
//        $this->email->from($this->config->item('correo_general_empresa'), $this->config->item('nombre_empresa'), $this->config->item('correo_general_empresa'));
//
//        $data = array(
//            'username' => ucwords($datos_email['nombre_cliente']),
//            'email' => $datos_email['email_cliente'],
//            'mensaje' => $datos_email['mensaje_email'], //Es un array de párrafos
//            'empresa' => $datos_email['empresa_cliente'],
//        );
//
//        $this->email->to($datos_email['email_representante']);
//        if( isset($datos_email['email_cliente']) && $datos_email['email_cliente'] != datos_email['email_representante'] )
//        {
//            $this->email->cc($datos_email['email_cliente']);
//        }
//        $this->email->bcc($this->config->item('correos_bcc'));
//
//        $this->email->subject($datos_email['asunto_email']); // replace it with relevant subject
//        $body = $this->load->view('emails/general_template.php', $data, TRUE);
//
//        //enviar correo
//        $this->email->message($body);
//
//        if (!$this->email->send())
//        {
//            ob_start();
//            $this->email->print_debugger();
//            $error = ob_end_clean();
//            $errors[] = $error;
//
//            return false;
//        }
//        else
//        {
//            return true;
//        }
    }

}
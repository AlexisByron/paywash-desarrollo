<?php

class MY_Model extends CI_Model {

    public function get_unique($table, $where = array())
    {
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get_where($table, $where, 1);
        $return = $q->result_array();
        if(empty($return))
        {
            return null;
        }
        else
        {
            return $return[0];
        }
    }

    public function get($table, $fields = null, $where = null)
    {
        if(isset($fields))
        {
            $this->db->select($fields);
        }
        if(isset($where))
        {
            $this->db->where($where);
        }
        $q = $this->db->get($table);
        $result = $q->result_array();
        return !empty($result) ? $result : null;
    }

    public function get_where($table, $where = array())
    {
        $q = $this->db->get_where($table, $where);
        $return = $q->result_array();
        if(empty($return))
        {
            return null;
        }
        else
        {
            return $return;
        }
    }

    public function getAmount($table, $where = array())
    {
        $q = $this->db->get_where($table, $where);
        $result = $q->num_rows();
        return $result;
    }

    public function insert($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $data, $where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        if( $this->db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function appendToField($table, $field, $where, $data)
    {
        $this->db->select($field);
        $q = $this->db->get_where($table, $where)->result_array();
        if(strlen($q[0][$field]) > 0)
        {
            $field_data_array = explode(',', $q[0][$field]);
            array_push($field_data_array, $data);
            $field_data = implode(',', $field_data_array);
        }
        else
        {
            $field_data = $data;
        }

        if( $this->update($table, array($field => $field_data), $where) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function removeToField($table, $field, $where, $data)
    {
        $this->db->select($field);
        $q = $this->db->get_where($table, $where)->result_array();
        $datos_comma_separated = $q[0][$field];
        $datos_array = explode(',', $datos_comma_separated);
        if(($key = array_search($data, $datos_array)) !== false) unset($datos_array[$key]); //Elimina el elemento del array que contiene el valor $data
        $nuevo_datos_comma_separated = implode(',', $datos_array);

        if( $this->update($table, array($field => $nuevo_datos_comma_separated), $where) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getCotizacionByID($order_id)
    {
        $q = $this->db->get_where('cotizaciones_recibidas', array('order_id' => $order_id), 1);
        $return = $q->result_array();
        if(!empty($return))
        {
            return $return[0];
        }
        else
        {
            return false;
        }

    }

    public function delete($table, $where)
    {
        $this->db->where( $where );
        $this->db->delete( $table );
    }

}
<?php

class Itr_model extends CI_Model
{

    public function get_itrs()
    {
        $this->db->select('*');
        $query = $this->db->get('itrs');
        return $query->result();
    }

    public function get_itrs_for_garantia()
    {
        $this->db->select('identificador, sector');
        $query = $this->db->get('itrs');
        return $query->result();
    }


    public function get_itrs_filtered($length, $offset)
    {
        $this->db->select('*');
        $query = $this->db->get('itrs', $length, $offset);
        return $query->result();
    }

    public function get_itrs_amount() {
        $query = $this->db->from("itrs")->count_all_results();
        return $query;
    }

    public function insert_itr($input)
    {
        $itr_registrado = $this->db->insert('itrs', $input);
        return $itr_registrado ? $itr_registrado : null;
    }

    public function get_itrs_list_names() {
        return $this->db->list_fields('itrs');
    }

    public function get_itrs_search($column_name, $search_string) {
        $this->db->select('*');
        $this->db->like($column_name, $search_string, 'both');
        $query = $this->db->get('itrs');

        return $query->result();
    }


}
<?php $fecha_actual = date('Y/m/d') ?>
<?php $concesionario=(isset($_SESSION['user']['concesionario']))?$_SESSION['user']['concesionario']:''; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Lavados</title>

    <!-- vendor css -->
<!--    <link href="--><?php //echo base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/ionicons/css/ionicons.min.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/highlightjs/styles/github.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/select2/css/select2.min.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/timepicker/jquery.timepicker.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/spectrum-colorpicker/spectrum.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') ?><!--" rel="stylesheet">-->
<!---->
<!--    <link href="--><?php //echo base_url('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/css/dataTables.customLoader.circle.css') ?><!--" rel="stylesheet">-->
<!--    <link href="--><?php //echo base_url('assets/css/dataTables.customLoader.circle.css') ?><!--" rel="stylesheet">-->
    <!-- Bracket CSS -->
<!--    <link rel="stylesheet" href="--><?php //echo base_url('assets/css/bracket.css') ?><!--">-->
<!--    <link rel="stylesheet" href="--><?php //echo base_url('assets/css/alexis.css') ?><!--">-->
<!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">-->
<!--    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->
<!--    -->
<!--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->

    <link href="<?php echo base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/highlightjs/styles/github.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/select2/css/select2.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/dataTables.customLoader.circle.css') ?>" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bracket.css') ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/alexis.css') ?>">

    <style>

       


#preloader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #fff;
    /* change if the mask should have another color then white */
    z-index: 99;
    /* makes sure it stays on top */
}

#status {
    width: 200px;
    height: 200px;
    position: absolute;
    left: 50%;
    /* centers the loading animation horizontally one the screen */
    top: 50%;
    /* centers the loading animation vertically one the screen */
    background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
    /* path to your loading animation */
    background-repeat: no-repeat;
    background-position: center;
    margin: -100px 0 0 -100px;
    /* is width and height divided by two */
}


    height: 40px;
    }

 h6 {
        transition: 0.4s;
        color: #0038f0;
        font-size: 20px;
        text-decoration: none;
        padding: 0 10px;
        margin: 0 10px;
      }
      /*h6:hover {
        background-color: #0038f0;
        color: white;
        padding: 10px 10px;
      }*/

      .activo{
        background-color:#0038f0!important;
        color: white;
        /*padding: 10px 10px;*/ 
      }
  
      a:link
    {
    text-decoration:none;
    } 

   
</style>


</head>

<body>


<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""></a></div>
<div class="br-sideleft sideleft-scrollbar">
    <h6 class="text-center">Desarrollo</h6>
    <br>
    <div class="row">

        <div class="logo">
            <img src="<?php echo base_url() ?>/assets/imgs/group23.gif" alt="no esta" class="logoP">
        </div>

    </div>
    <br><br>

    <li class="br-menu-item  " >
        <a href="<?php echo base_url() ?>inicio/home" class="br-menu-link">
            <img src="<?php echo base_url() ?>/assets/imgs/home.png" class="wd-32 rounded-circle" alt="">
            <span class="menu-item-label label">Home</span>
        </a>
    </li>

    <li class="br-menu-item marcado" >
        <a href="<?php echo base_url() ?>inicio/lavados" class="br-menu-link">
            <img src="<?php echo base_url() ?>/assets/imgs/lavado-activo.png" class="wd-32 rounded-circle" alt="">
            <span class="menu-item-label labelMarcado">Lavados y Secados</span>
        </a>
    </li>



    <?php if ($_SESSION['user']['perfil']=="operario") { ?>
        <li class="br-menu-item " >
            <a href="<?php echo base_url() ?>inicio/historial" class="br-menu-link">
                <img src="<?php echo base_url() ?>/assets/imgs/lavados.png" class="wd-32 rounded-circle" alt="">
                <span class="menu-item-label label">Historial</span>
            </a>
        </li>

    <?php } ?>


    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">
         
    <div class="header-left">
             <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        </div>
            

    </div><!-- br-header-left -->

    <div class="br-header-right">
        <nav class="nav">



            <div class="dropdown">
                <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                    <img src="<?php echo base_url() ?>/assets/imgs/notification.png"  alt="">
                    <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                    <!-- end: if statement -->
                </a>

            </div><!-- dropdown -->

            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"></span>
                    <img src="<?php echo base_url() ?>/assets/imgs/user.png" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                    <div class="dropdown-menu dropdown-menu-header wd-250">
                        <li class="text-center"><?php print_r($_SESSION['user']['user']  );?></li>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">

                        <li><a href="<?php echo base_url('Sign/logout') ?>"><i class="icon ion-power"></i> Salir</a></li>
                    </ul>
                    <hr>

                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>

    </div><!-- br-header-right -->
</div>
<!-- ########## END: HEAD PANEL ########## -->



<!-- ########## START: MAIN PANEL ########## -->
<div >

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">


    <div class="row FilaIndicadores">
        <div  class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
            <h2 class="valor" id="lavados-totales">0</h2>
            <h4 class="indicador">Lavados totales</h4>
        </div>
        <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xs-5 col-xl-2">
            <h2 class="valor" id="secados-totales">0</h2>
            <h4 class="indicador">Secados totales</h4>
        </div>
        <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
            <h2 class="valor" id="recaudacion">0</h2>
            <h4 class="indicador">Recaudación total</h4>
        </div>
        <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
            <h2 class="valor" id="fallas-totales">0</h2>
            <h4 class="indicador">Fallas totales</h4>
        </div>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper Contenedor" >

            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12  col-xl-6 col-md-12">
                    <h3>LAVADOS Y SECADOS</h3>
                </div>
                <div class="col-12 col-sm-12 col-lg-12  col-xl-6 col-md-12">
                    <div class="row">
                        <div class="col-3">
                            <a href="" class="TempLavados picker_tiempo" data-class="week" id="week"><h5 >ESTA SEMANA</h5></a>
                        </div>
                        <div class="col-3">
                            <a href="" class="TempLavados picker_tiempo" data-class="month" id="month"><h5 class="seleccionado">ESTE MES</h5></a>
                        </div>
                        <div class="col-3">
                            <a href="" class="TempLavados picker_tiempo" data-class="year" id="year"><h5 >ESTE AÑO</h5></a>
                        </div>
                        <div class="col-3" >
                            <a href="" class="TempLavados" data-toggle="modal" data-target="#exampleModal"><img src="<?php echo base_url() ?>/assets/imgs/calendar.png"  alt="calendario"></a>
                        </div>
                    </div>
                </div>
            </div><br>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                <div class="modal-dialog" role="document" style="margin: auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Filtrado por fechas</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="6">
                                        <h6>Fecha Inicio</h6>
                                    </div>
                                    <div class="6">
                                        <input type="date" class="form-control" name="Finicio"/>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="6">
                                        <h6>Fecha termino</h6>
                                    </div>
                                    <div class="6">
                                        <input type="date" class="form-control" name="Ftermino"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="buscarByDate">Buscar</button>
                        </div>
                    </div>
                </div>
            </div>


               <div class="grid ">
                    <div class="row">
                        <br>
                        <!-- 
                        <div class="col-12 col-md-12 col-xl-6 col-sm-12 tablas"  >

                            <div class="row" style="margin-top:20px;">

                                <div class="col-8" style="color:#37db11">
                                    <h5> Lavados por concesionario</h5>
                                </div>
                                <div class="col-4 total"  >

                                    <h5 >999.999.999 Total</h5>
                                </div>
                            </div><br>
                            
                            
                            <div class="table-wrapper">
                             <table id="datatable1" class="table display responsive nowrap" style="margin-bottom:0px">
                                  <thead>
                                    <tr>
                                      <th class="wd-15p">Concesionario</th>
                                      <th class="wd-15p">N° de lavados</th>

                                      <th class="wd-15p">Ranking</th>

                                    </tr>
                                  </thead>
                                  <tbody>

                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-angle-up fa-2x" ></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-minus fa-2x"></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-angle-down fa-2x"></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-minus fa-2x"></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-angle-down fa-2x"></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-angle-down fa-2x"></i></td>

                                </tr>
                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-angle-up fa-2x" ></td>

                                </tr>

                                <tr>
                                    <td>Consesionario x</td>
                                    <td><strong>3.453</strong></td>
                                    <td><i class="fas fa-minus fa-2x"></i></td>

                                </tr>

                                </tbody>
                                </table><br>
                            </div>
                        </div>

                <div class="col-12 col-md-12 col-xl-6 col-sm-12 tablas"  >
               
                <div class="row" style="margin-top:20px;">
                
                                <div class="col-8" style="color:#37db11">
                                     <h5> Lavados por ciudad</h5>
                                </div>
                                <div class="col-4 total"  >
                                
                                    <h5 >999.999.999 Total</h5>
                                </div>
                            </div><br>
                <table id="table2"  style="margin-bottom: 0px" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                 <th class="wd-15p">concesionario</th>
                                 <th class="wd-15p">n° de lavados</th>
                                  <th class="wd-15p">ranking</th>
                              </tr>
                            </thead>


                     </table><br>
                </div>-->
                <div class="col-12 col-md-12 col-xl-6 col-sm-12 tablas"  >
               
               <div class="row" style="margin-top:20px;">
               
                               <div class="col-8" style="color:#37db11">
                                    <h5>por comuna</h5>
                               </div>

                           </div>
               <table id="table"  style="margin-bottom: 0px" class="table display responsive nowrap">
                           <thead>
                             <tr>
                             <th class="wd-15p">Comuna</th>
                             <th class="wd-15p">N° de lavados</th>
                             <th class="wd-15p">N° de secados</th>
                              <th class="wd-15p">Exel</th>

                             </tr>
                           </thead>


                    </table><br>
               </div>
               <div class="col-12 col-md-12 col-xl-6 col-sm-12 tablas"  >
               
               <div class="row" style="margin-top:20px;">
               
                               <div class="col-8" style="color:#37db11">
                                    <h5>por edificio</h5>
                               </div>

                           </div>
               <table id="table2"  style="margin-bottom: 0px" class="table display responsive nowrap">
                           <thead>
                             <tr>
                             <th class="wd-15p">Edificio</th>
                            <th class="wd-15p">N° de lavados</th>
                                 <th class="wd-15p">N° de secados</th>
                                 <th class="wd-15p">exel</th>

                             </tr>
                           </thead>

                    </table><br>
               </div>

                
               </div>
           
               </div><!-- br-mainpanel -->

</div>
<footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2019. Marsol. Todos los derechos reservados.</div>

        </div>
        <div class="footer-right d-flex align-items-center">
        </div>
    </footer>

<!-- ########## END: MAIN PANEL ########## -->

<script src="<?php echo base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery-ui/ui/widgets/datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/perfect-scrollbar/perfect-scrollbar.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/peity/jquery.peity.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/highlightjs/highlight.pack.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery-steps/build/jquery.steps.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/parsleyjs/parsley.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/select2/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.maskedinput/jquery.maskedinput.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/ion-rangeslider/js/ion.rangeSlider.min.js') ?>"></script>

<script src="<?php echo base_url('assets/lib/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') ?>"></script>

<script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.flash.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/pdfmake.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/vfs_fonts.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.print.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/datatables.net-buttons/dataTables.buttons.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/bracket.js') ?>"></script>
<script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script>
    $(window).on('load', function () { // makes sure the whole site is loaded
        $('#status').fadeOut('fast'); // will first fade out the loading animation
        $('#preloader').fadeOut('fast'); // will fade out the white DIV that covers the website.
    })
</script>

<script>
    function endOfWeek(date)
    {

        var lastday = date.getDate() - (date.getDay() - 1) + 6;
        return new Date(date.setDate(lastday));

    }
    function startOfWeek(date)
    {
        // (date.getDate()-date.getDay())+1
        var firstday = date.getDate() - (date.getDay() -1) ;
        return new Date(date.setDate(firstday));

    }

        $( document ).ready(function() {

            let comunas=[];
            let edificios=[];
            var concesionario='<?php echo $concesionario;?>';

            // console.log(concesionario);
            var reporte="mensual";

            //filtros para semana
            var date = new Date();
            // var termino_semana=endOfWeek(date).toISOString().split('T')[0]+"T23:59:59.000-03:00";//devuelve el ultimo dia de la semana

            date = new Date(); //vuelvo a setear la fecha al inicio
            // var inicio_semana=startOfWeek(date).toISOString().split('T')[0]+"T00:00:00.000-03:00";//devuelve el ultimo dia de la semana

            // console.log(inicio_semana);
            // console.log(termino_semana);

            //fin filtros semana

            //inicio filtro mes
            date = new Date();
            var inicio_mes=new Date(date.setDate("01")).toISOString().split('T')[0]+"T00:00:00.000-03:00";
            // console.log(inicio_mes);
            date = new Date();
            var termino_mes=new Date(date.getFullYear(), date.getMonth() + 1, 0).toISOString().split('T')[0]+"T23:59:59.000-03:00";
            // console.log(termino_mes);
            //fin filtro mes

            //filtros inicio año
            date = new Date();
            var inicio_anio= new Date(date.setMonth("0"));
            inicio_anio=new Date(inicio_anio.setDate("01")).toISOString().split('T')[0]+"T00:00:00.000-03:00";
            // console.log(inicio_anio);
            date = new Date();
            var fin_anio= new Date(date.setMonth("11"));
            var fin_anio=new Date(fin_anio.getFullYear(), fin_anio.getMonth() + 1, 0).toISOString().split('T')[0]+"T23:59:59.000-03:00";
            // fin_anio=new Date(fin_anio.setDate("31")).toISOString().split('T')[0]+"T23:59:59.000-03:00";
            // console.log(fin_anio);
            //fin filtros año



            $.ajax({

                data: {"concesionario" :  concesionario, "tipo_reporte":"semanal", "desde":inicio_mes ,"hasta":termino_mes},

                type: "post",

                dataType: "json",

                url:"https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte/count",

            })
                .done(function( result, textStatus, jqXHR ) {
                    console.log(result);

                    document.getElementById("lavados-totales").textContent = result["data"]["total_lavadoras"];
                    document.getElementById("secados-totales").textContent = result["data"]["total_secadoras"];
                    document.getElementById("recaudacion").textContent = result["data"]["recaudado"];
                    document.getElementById("fallas-totales").textContent = result["data"]["fallas"];
                    var totalC=0;
                    var totalE=0;

                    for(let key in result.data.comunas){
                        var existe=false;
                        var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte="+reporte+"&comuna="+encodeURIComponent(result.data.comunas[key].nombre.trim());
                        console.log(url);
                        for(let key2 in comunas){
                            if(comunas[key2][0]==result.data.comunas[key].nombre){
                                existe=true;
                                comunas[key2][1]+=result.data.comunas[key].cantidad_lavadora;
                                comunas[key2][2]+=result.data.comunas[key].cantidad_secadora;
                            }

                        }

                        if (!existe) {
                            comunas.push([result.data.comunas[key].nombre,result.data.comunas[key].cantidad_lavadora,result.data.comunas[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                        }
                    }
                    for(let key in result.data.edificios){
                        var existe=false;

                        var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte="+reporte+"&edificio="+encodeURIComponent(result.data.edificios[key].nombre.trim());
                        console.log(url);
                        for(let key2 in edificios){
                            if(edificios[key2][0]==result.data.edificios[key].nombre){
                                existe=true;
                                edificios[key2][1]+=result.data.edificios[key].cantidad_lavadora;
                                edificios[key2][2]+=result.data.edificios[key].cantidad_secadora;
                            }
                        }
                        if (!existe) {
                            edificios.push([result.data.edificios[key].nombre,result.data.edificios[key].cantidad_lavadora,result.data.edificios[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                        }
                    }
                    // document.getElementById("totalC").textContent = totalC+" total";
                    // document.getElementById("totalE").textContent = totalE+" total";

                    // console.log(comunas);
                    // console.log(edificios);

                    $('#table').DataTable({
                        // dom: 'Bflrtip',
                        responsive: true,
                        "scrollY": "300px",
                        "scrollCollapse": true,
                        "paging":         false,
                        "bFilter": true,
                        "bInfo": false,
                        data:comunas

                })

                    $('#table2').DataTable({
                        // dom: 'Bflrtip',
                        responsive: true,
                        "scrollY": "300px",
                        "scrollCollapse": true,
                        "paging":         false,
                        "bFilter": true,
                        "bInfo": false,
                        data:edificios
                    })


                .fail(function( jqXHR, textStatus, errorThrown ) {
                    if ( console && console.log ) {
                        console.log( "La solicitud a fallado: " +  textStatus);
                    }
                    swal("Error", "No se encontraron datos", "error");
                });
        });




        });





    </script>

<script>

        $(".TempLavados").click(function(e){
            $(".TempLavados").find('h5').removeClass('seleccionado');
            e.preventDefault();
            console.log(e);
            $(this).find('h5').addClass('seleccionado');
        })
    </script>

<script>

            $('.picker_tiempo').click(function (e) {

                var filtro = ($(this).attr('data-class'));

                var inicio;
                var termino;
                var Treporte="";
                switch (filtro) {
                    case "week":Treporte="semanal";
                        var date = new Date();
                        var termino=endOfWeek(date).toISOString().split('T')[0]+"T23:59:59.000-03:00";//devuelve el ultimo dia de la semana

                        date = new Date(); //vuelvo a setear la fecha al inicio
                        var inicio=startOfWeek(date).toISOString().split('T')[0]+"T00:00:00.000-03:00";//devuelve el primer dia de la semana
                        break;
                    case "month":Treporte="mensual";
                        date = new Date();
                        var inicio=new Date(date.setDate("01")).toISOString().split('T')[0]+"T00:00:00.000-03:00";
                        date = new Date();
                        termino =new Date(date.getFullYear(), date.getMonth() + 1, 0).toISOString().split('T')[0]+"T23:59:59.000-03:00";
                        // var termino=new Date(date.setDate("31")).toISOString().split('T')[0]+"T23:59:59.000-03:00";
                        break;
                    case "year":Treporte="anual";
                        date = new Date();
                        var inicio_anio= new Date(date.setMonth("0"));
                        inicio=new Date(inicio_anio.setDate("01")).toISOString().split('T')[0]+"T00:00:00.000-03:00";

                        date = new Date();
                        var fin_anio= new Date(date.setMonth("11"));
                        termino =new Date(fin_anio.getFullYear(), fin_anio.getMonth() + 1, 0).toISOString().split('T')[0]+"T23:59:59.000-03:00";

                        break;
                }

                let comunas = [];
                let edificios = [];
                var concesionario = '<?php echo $concesionario;?>';
                var reporte=Treporte;

                $("#table").dataTable().fnDestroy();
                $("#table2").dataTable().fnDestroy();
                $.ajax({

                    data: {"concesionario" :  concesionario,"tipo_reporte":"semanal","desde":inicio,"hasta":termino},

                    type: "post",

                    dataType: "json",

                    url:"https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte/count",

                })
                    .done(function( result, textStatus, jqXHR ) {
                        console.log(result);

                        document.getElementById("lavados-totales").textContent = result["data"]["total_lavadoras"];
                        document.getElementById("secados-totales").textContent = result["data"]["total_secadoras"];
                        document.getElementById("recaudacion").textContent = result["data"]["recaudado"];
                        document.getElementById("fallas-totales").textContent = result["data"]["fallas"];
                        var totalC=0;
                        var totalE=0;

                        for(let key in result.data.comunas){
                            var existe=false;
                            var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte="+reporte+"&comuna="+encodeURIComponent(result.data.comunas[key].nombre.trim());

                            for(let key2 in comunas){
                                if(comunas[key2][0]==result.data.comunas[key].nombre){
                                    existe=true;
                                    comunas[key2][1]+=result.data.comunas[key].cantidad_lavadora;
                                    comunas[key2][2]+=result.data.comunas[key].cantidad_secadora;
                                }

                            }

                            if (!existe) {
                                comunas.push([result.data.comunas[key].nombre,result.data.comunas[key].cantidad_lavadora,result.data.comunas[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                            }
                        }
                        for(let key in result.data.edificios){
                            var existe=false;

                            var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte="+reporte+"&edificio="+encodeURIComponent(result.data.edificios[key].nombre.trim());
                            for(let key2 in edificios){
                                if(edificios[key2][0]==result.data.edificios[key].nombre){
                                    existe=true;
                                    edificios[key2][1]+=result.data.edificios[key].cantidad_lavadora;
                                    edificios[key2][2]+=result.data.edificios[key].cantidad_secadora;
                                }
                            }
                            if (!existe) {
                                edificios.push([result.data.edificios[key].nombre,result.data.edificios[key].cantidad_lavadora,result.data.edificios[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                            }
                        }
                        // document.getElementById("totalC").textContent = totalC+" total";
                        // document.getElementById("totalE").textContent = totalE+" total";
                        console.log("OK");
                        console.log(comunas);
                        console.log(edificios);

                        $('#table').DataTable({
                            // dom: 'Bflrtip',
                            responsive: true,
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging":         false,
                            "bFilter": true,
                            "bInfo": false,
                            data:comunas

                        })

                        $('#table2').DataTable({
                            // dom: 'Bflrtip',
                            responsive: true,
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging":         false,
                            "bFilter": true,
                            "bInfo": false,
                            data:edificios
                        })


                            .fail(function( jqXHR, textStatus, errorThrown ) {
                                if ( console && console.log ) {
                                    console.log( "La solicitud a fallado: " +  textStatus);
                                }
                                swal("Error", "No se encontraron datos", "error");
                            });
                    });



            });

        </script>

<script >
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })

        $('#buscarByDate').click(function () {
            var Finicio =$("input[name=Finicio]").val();
            var Ftermino =$("input[name=Ftermino]").val();
            if(Finicio!="" && Ftermino!=""){
                if(Ftermino<Finicio){
                    swal("Error", "La fecha de termino no puede ser menor que la de inicio \n fecha inicio: "+Finicio+" fecha termino: "+Ftermino, "error");;
                }else{
                    console.log(Finicio+" "+Ftermino);
                    var desde=Finicio.split("-");
                    var hasta=Ftermino.split("-");
                    // var desde1=new Date(desde[0],desde[1]-1,desde[2]);
                    // var hasta1=new Date(hasta[0],hasta[1]-1,hasta[2]);

                    var desde1= (desde[0]+"-"+(desde[1])+"-"+desde[2]+"T00:00:00.000-03:00");
                    var hasta1 =(hasta[0]+"-"+(hasta[1])+"-"+hasta[2]+"T23:59:59.000-03:00");


                    var concesionario='<?php echo $concesionario;?>';

                    $.ajax({

                        data: {"concesionario" :  concesionario , "tipo_reporte":"semanal" ,"desde":desde1 ,"hasta":hasta1},

                        type: "post",

                        dataType: "json",

                        url:"https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte/count",

                    })
                        .done(function( result, textStatus, jqXHR ) {
                            console.log(result);
                            let comunas=[];
                            let edificios=[];
                            $("#table").dataTable().fnDestroy();
                            $("#table2").dataTable().fnDestroy();
                            document.getElementById("lavados-totales").textContent = result["data"]["total_lavadoras"];
                            document.getElementById("secados-totales").textContent = result["data"]["total_secadoras"];
                            document.getElementById("recaudacion").textContent = result["data"]["recaudado"];
                            document.getElementById("fallas-totales").textContent = result["data"]["fallas"];
                            var totalC=0;
                            var totalE=0;

                            for(let key in result.data.comunas){
                                var existe=false;
                                var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte=semanal&desde="+desde1.toISOString()+"&hasta="+hasta1.toISOString()+"&comuna="+encodeURIComponent(result.data.comunas[key].nombre.trim());
                                    console.log(url);
                                for(let key2 in comunas){
                                    if(comunas[key2][0]==result.data.comunas[key].nombre){
                                        existe=true;
                                        comunas[key2][1]+=result.data.comunas[key].cantidad_lavadora;
                                        comunas[key2][2]+=result.data.comunas[key].cantidad_secadora;
                                    }

                                }

                                if (!existe) {
                                    comunas.push([result.data.comunas[key].nombre,result.data.comunas[key].cantidad_lavadora,result.data.comunas[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                                }
                            }
                            for(let key in result.data.edificios){
                                var existe=false;
                                var url="https://paywash.tecnoandina.cl:1881/api/paywash/concesionario/reporte?concesionario="+concesionario+"&tipo_reporte=semanal&desde="+desde1.toISOString()+"&hasta="+hasta1.toISOString()+"&edificio="+encodeURIComponent(result.data.edificios[key].nombre.trim());

                                for(let key2 in edificios){
                                    if(edificios[key2][0]==result.data.edificios[key].nombre){
                                        existe=true;
                                        edificios[key2][1]+=result.data.edificios[key].cantidad_lavadora;
                                        edificios[key2][2]+=result.data.edificios[key].cantidad_secadora;
                                    }
                                }
                                if (!existe) {
                                    edificios.push([result.data.edificios[key].nombre,result.data.edificios[key].cantidad_lavadora,result.data.edificios[key].cantidad_secadora,'<a href='+url+'><img src="<?php echo base_url('assets/imgs/excel.png')?>" alt="descargar" style="height:20px"></a>']);
                                }
                            }
                            // document.getElementById("totalC").textContent = totalC+" total";
                            // document.getElementById("totalE").textContent = totalE+" total";

                            console.log(comunas);
                            console.log(edificios);

                            $('#table').DataTable({
                                // dom: 'Bflrtip',
                                responsive: true,
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "paging":         false,
                                "bFilter": true,
                                "bInfo": false,
                                data:comunas

                            })

                            $('#table2').DataTable({
                                // dom: 'Bflrtip',
                                responsive: true,
                                "scrollY": "300px",
                                "scrollCollapse": true,
                                "paging":         false,
                                "bFilter": true,
                                "bInfo": false,
                                data:edificios
                            })



                        })
                        .fail(function( jqXHR, textStatus, errorThrown ) {
                            if ( console && console.log ) {
                                console.log( "La solicitud a fallado: " +  textStatus);
                            }
                            swal("Error", "No se encontraron datos", "error");
                        });
                }
            }else{
                swal("Error", "Ingrese fecha de inicio y termino para realizar busqueda", "error");
            }


        })


</script>
        <script >
            if ( $('[type="date"]').prop('type') != 'date' ) {
                $('[type="date"]').datepicker();
            }
        </script>

</body>
</html>

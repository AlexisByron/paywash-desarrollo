<?php $fecha_actual = date('Y/m/d') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Lavados</title>

    <!-- vendor css -->
    <link href="<?php echo base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/highlightjs/styles/github.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/select2/css/select2.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/dataTables.customLoader.circle.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/dataTables.customLoader.circle.css') ?>" rel="stylesheet">
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bracket.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/alexis.css') ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>




        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #fff;
            /* change if the mask should have another color then white */
            z-index: 99;
            /* makes sure it stays on top */
        }

        #status {
            width: 200px;
            height: 200px;
            position: absolute;
            left: 50%;
            /* centers the loading animation horizontally one the screen */
            top: 50%;
            /* centers the loading animation vertically one the screen */
            background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
            /* path to your loading animation */
            background-repeat: no-repeat;
            background-position: center;
            margin: -100px 0 0 -100px;
            /* is width and height divided by two */
        }


        height: 40px;
        }

        h6 {
            transition: 0.4s;
            color: #0038f0;
            font-size: 20px;
            text-decoration: none;
            padding: 0 10px;
            margin: 0 10px;
        }
        /*h6:hover {
          background-color: #0038f0;
          color: white;
          padding: 10px 10px;
        }*/

        .activo{
            background-color:#0038f0!important;
            color: white;
            /*padding: 10px 10px;*/
        }

        a:link
        {
            text-decoration:none;
        }


    </style>


</head>

<body>


<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""></a></div>
<div class="br-sideleft sideleft-scrollbar">
    <br>
    <div class="row">

        <div class="logo">
            <img src="<?php echo base_url() ?>/assets/imgs/group23.gif" alt="no esta" class="logoP">
        </div>

    </div>
    <br><br>

    <li class="br-menu-item  " >
        <a href="<?php echo base_url() ?>inicio/home" class="br-menu-link">
            <img src="<?php echo base_url() ?>/assets/imgs/home.png" class="wd-32 rounded-circle" alt="">
            <span class="menu-item-label label">Home</span>
        </a>
    </li>

    <li class="br-menu-item " >
        <a href="<?php echo base_url() ?>inicio/lavados" class="br-menu-link">
            <img src="<?php echo base_url() ?>/assets/imgs/lavados.png" class="wd-32 rounded-circle" alt="">
            <span class="menu-item-label label">Lavados</span>
        </a>
    </li>




    <?php if ($_SESSION['user']['perfil']=="operario") { ?>
        <li class="br-menu-item marcado" >
            <a href="<?php echo base_url() ?>inicio/historial" class="br-menu-link">
                <img src="<?php echo base_url() ?>/assets/imgs/lavado-activo.png" class="wd-32 rounded-circle" alt="">
                <span class="menu-item-label labelMarcado">Historial</span>
            </a>
        </li>
    <?php } ?>


    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">

        <div class="header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        </div>


    </div><!-- br-header-left -->

    <div class="br-header-right">
        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                    <img src="<?php echo base_url() ?>/assets/imgs/notification.png"  alt="">
                    <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                    <!-- end: if statement -->
                </a>

            </div><!-- dropdown -->

            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"></span>
                    <img src="<?php echo base_url() ?>/assets/imgs/user.png" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <li class="text-center"><?php print_r($_SESSION['user']["user"]);?></li>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">

                        <li><a href="<?php echo base_url('Sign/logout') ?>"><i class="icon ion-power"></i> Salir</a></li>
                    </ul>
                    <hr>

                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>

    </div><!-- br-header-right -->
</div>
<!-- ########## END: HEAD PANEL ########## -->



<!-- ########## START: MAIN PANEL ########## -->
<div >

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="row FilaIndicadores">
            <div  class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
                <h2 class="valor" id="lavados-totales"></h2>
                <h4 class="indicador"></h4>
            </div>
            <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xs-5 col-xl-2">
                <h2 class="valor" id="secados-totales"></h2>
                <h4 class="indicador"></h4>
            </div>
            <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
                <h2 class="valor" id="recaudacion"></h2>
                <h4 class="indicador"></h4>
            </div>
            <div style="margin-left: auto;" class="col-6 col-lg-2 col-md-2 col-sm-5 col-xl-2 col-xs-5">
                <h2 class="valor" id="fallas-totales"></h2>
                <h4 class="indicador"></h4>
            </div>
        </div>
        <div class="br-pagebody">
            <div class="br-section-wrapper Contenedor" >
                <div class="grid ">
                    <div class="col-12 col-md-12 col-xl-12 col-sm-12 tablas"  >

                        <table id="example" class="display dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">

                           <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1"></th>
                                    <th rowspan="1" colspan="1"></th>
                                    <th rowspan="1" colspan="1"></th>
                                    <th rowspan="1" colspan="1"></th>
                                    <th rowspan="1" colspan="1">
                                        <select>
                                            <option value=""></option>
                                            <option value="10001">10001</option>
                                            <option value="10002">10002</option>
                                            <option value="10003">10003</option>
                                            <option value="10004">10004</option>
                                        </select>
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        <select>
                                            <option value=""></option>
                                            <option value="100">100</option>
                                            <option value="1500">1500</option>
                                            <option value="1700">1700</option>
                                        </select>
                                    </th>
                                    <th rowspan="1" colspan="1"><input type="date"></th>
                                    <th rowspan="1" colspan="1">
                                        <select>
                                            <option value=""></option>
                                            <option value="Activado">Activado</option>
                                        </select>
                                    </th>
                                </tr>
                           </tfoot>

                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc " tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="_id: activate to sort column descending" style="width: 202px;">_id</th>
                                    <th class="sorting wd-15p" style="width: 100px" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Nombre: activate to sort column ascending" style="width: 202px;">Nombre</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Apellido: activate to sort column ascending" style="width: 202px;">Apellido</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="RUT: activate to sort column ascending" style="width: 202px;">RUT</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Serial Lavadora: activate to sort column ascending" style="width: 202px;">Serial Lavadora</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Precio: activate to sort column ascending" style="width: 202px;">Precio</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Fecha de pago: activate to sort column ascending" style="width: 202px;">Fecha de pago</th>
                                    <th class="sorting wd-15p" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Estado: activate to sort column ascending" style="width: 202px;">Estado</th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>

                        </table>


                    </div>

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" >Ingrese número del equipo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body" id="opciones" >

                                    <div class="row ">
                                        <div class="col-6 col-xs-12"><input type="number" class="form-control" placeholder="N°del equipo" id="id-lavadora" minlength="1" maxlength="5"></div>
                                        <div class="col-6 col-xs-12"><button type="button" class="btn btn-primary btn-lg" style="width: 100%" id="Reinicio">Reemplazar</button></div>
                                    </div>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <footer class="br-footer">
            <div class="footer-left">
                <div class="mg-b-2">Copyright &copy; 2019. Marsol. Todos los derechos reservados.</div>
            </div>
            <div class="footer-right d-flex align-items-center">
            </div>
        </footer>
    </div><!-- br-mainpanel -->
</div>


<!-- ########## END: MAIN PANEL ########## -->
<div>
    <script src="<?php echo base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/jquery-ui/ui/widgets/datepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/perfect-scrollbar/perfect-scrollbar.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/moment/min/moment.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/peity/jquery.peity.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/highlightjs/highlight.pack.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/jquery-steps/build/jquery.steps.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/parsleyjs/parsley.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/select2/js/select2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/jquery.maskedinput/jquery.maskedinput.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/ion-rangeslider/js/ion.rangeSlider.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/lib/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.flash.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/jszip.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/pdfmake.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/vfs_fonts.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.html5.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/buttons.print.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datatables.net-buttons/dataTables.buttons.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/bracket.js') ?>"></script>
    <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
</div>


<script>
    $(window).on('load', function () { // makes sure the whole site is loaded
        $('#status').fadeOut('fast'); // will first fade out the loading animation
        $('#preloader').fadeOut('fast'); // will fade out the white DIV that covers the website.
    })
</script>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable();
    } );
</script>


<script>
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        // $('#example tfoot th').each(function () {
        //     var title = $(this).text();
        //     if (title !== "") {
        //         $(this).html('<input type="text" placeholder="Buscar ' + title + '" />');
        //     }
        // });
        TablaHistorial();


        // Apply the search
        // table.columns().every(function () {
        //     var that = this;
        //     $('input', this.footer()).on('keyup change', function () {
        //         if(this.id!="id-lavadora"){
        //             if (that.search() !== this.value) {
        //                 console.log(that);
        //                 console.log(this.value);
        //                 that
        //                     .search(this.value)
        //                     .draw();
        //             }
        //         }
        //
        //     });
        // });
    });
    function TablaHistorial() {

        $("#example").dataTable().fnDestroy();
        var table =

            $('#example').DataTable({
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;

                        if (column[0][0] === 4 || column[0][0] === 5 || column[0][0] === 7) {


                            var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column
                                        .search(val)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, a) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        } else if (column[0][0] === 6) {
                            var select = $('<input type="date"/>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $(this).val()
                                    column
                                        .search(val)
                                        .draw();
                                });

                        }else{
                            var titulo="";

                            switch (column[0][0]) {
                                case 1:titulo=" nombre";
                                    break;
                                case 2:titulo="apellido";
                                    break;
                                case 3:titulo="rut";
                                    break;
                                default:titulo="";
                                    break;
                            }

                            var select = $('<input type="text" placeholder="buscar ' + titulo + '" />')
                                .appendTo($(column.footer()).empty())
                                .on('keyup change', function () {
                                    var val = $(this).val()
                                    column
                                        .search(val)
                                        .draw();
                                });
                        }
                    });

                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                responsive: true,
                "dom": "lrtip",
                "processing": false,
                "serverSide": true,
                "ajax": "https://paywash.tecnoandina.cl:1881/api/paywash/reporte/datatable_side",
                "columns": [
                    { title: "_id", "name": "_id" },
                    { title: "Nombre", "name": "nombre"  },
                    { title: "Apellido", "name": "apellido" },
                    { title: "Rut", "name": "rut" },
                    { title: "Serial Lavadora", "name": "id_lavadora" },
                    { title: "Precio", "name": "precio" },
                    { title: "Fecha de pago", "name": "fecha_pago" },
                    { title: "Estado", "name": "status_lavadora" },
                ],
                "columnDefs": [
                    {"targets": 0,
                        "render": function ( data, type, row, meta ) {
                            return data;
                        },
                        visible : false
                    },
                    {"targets": 3,
                        "render": function ( data, type, row, meta ) {
                            return data;
                        },
                        visible : false
                    },

                    {"targets": 6,
                        "render": function ( data, type, row, meta ) {
                            if(data!=""){
                                var date = convertUTCDateToLocalDate(new Date(data));
                                return date.toLocaleString();
                            }else{
                                return data
                            }

                        }
                    },
                    {"targets": 7,
                        responsivePriority: 1,
                        "render": function ( data, type, row, meta ) {

                            if(data=="No Activado"){
                                return data + "<button type=\"button\" class='btn btn-warning activar' style='margin-left: 10px' style='float: right;'  data-toggle=\"modal\" data-target=\"#exampleModal\" data-class='"+String(row[4])+"' data-pago=' "+String(row[0])+" ' ><img src=\"https://smartkitchen.cl/paywash/assets/imgs/refrescar.png\" alt=\"Reintentar\" style=\"height: 20px\"></button>";
                            }else{
                                return data;
                            }
                        }
                    },

                ]
            });
    }
</script>

<script>
    function convertUTCDateToLocalDate(date) {
        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();



        newDate.setHours(hours );

        return newDate;
    }
</script>

<script >
    var id;
    var idPago;
    $("body").on('click','.activar', function(){
         id=( $(this).attr('data-class') );
         idPago=( $(this).attr('data-pago') );

        document.getElementById("exampleModalLabel").innerHTML = "Desea Reactivar Esta o otra lavadora";
        $("#opciones").show();
        $("#formulario").hide();

        $("#esta").click(function(){

            reinicio(id,idPago);

        });

        $("#otra").click(function(){
            document.getElementById("exampleModalLabel").innerHTML = "Ingrese id de lavadora para reactivar";
            $("#formulario").show();
            $("#opciones").hide();

            $("#back").click(function(){
                document.getElementById("exampleModalLabel").innerHTML = "Desea Reactivar Esta o otra lavadora";
                $("#opciones").show();
                $("#formulario").hide();
            });





        });

    });
    $("#Reinicio").click(function(){
        $("#Reinicio").empty();
        $("#Reinicio").append("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Remplazando...");
        document.getElementById("Reinicio").disabled = true;
        console.log("click");
        var id = $('#id-lavadora').val();
        if(id.trim().length>0){

            reinicio(id,idPago);
        }else{
            swal("Error", "Para activar la lavadora es nescesario el id", "error");
        }


    });
</script>

<script >
    function reinicio(id,pago) {
       console.log("se ara una activacion de la lavadora :"+id+" por el pago de id: "+pago);
        $.ajax({
            data: {"serialEquipo" :  id.trim(), "razon":"prueba" ,_id:pago.trim()},
            type: "post",
            dataType: "json",
            url:"https://paywash.tecnoandina.cl:1881/api/paywash/pago/cambiar/equipo",
        })
            .done(function( result, textStatus, jqXHR ) {

               console.log(result);
                if(result.success && result.status==1){
                    swal("Activacion exitosa", "Se activo la lavadora: "+id +"\n" +" disponible para inicio", "success");
                    $("#Reinicio").empty();
                    $("#Reinicio").append("Reemplazar");
                    document.getElementById("Reinicio").disabled = false;
                    TablaHistorial();
                }else{
                    swal("Error", result.message, "error");
                }

                var cerrarModal = document.getElementById("close");
                cerrarModal.click();

            })
            .fail(function( jqXHR, textStatus, errorThrown ) {
                if ( console && console.log ) {
                //  console.log( "La solicitud a fallado: " +  textStatus);
                }
                swal("Error", "La solicitud falló", "error");

            });
    }
</script>



</body>
</html>

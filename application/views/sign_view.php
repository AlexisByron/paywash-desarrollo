<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">


    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Informes de trabajos realizados</title>


    <link href="<?php echo base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bracket.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/alexis.css') ?>">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

<?php echo isset($the_content) ? $the_content:'' ?>

<script src="<?php echo base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<script>
    $(function(){
        $('#loginDo').on('click', (evt)=>{
            console.log('Log Click');

            evt.preventDefault();
            $.ajax({
                url: "https://paywash.tecnoandina.cl:1881/api/paywash/login",
                // contentType : 'application/json',
                data:{"email":$('#usuario').val(),"password":$('#password').val(),"tipo":"dashboard"},
                type : 'POST'
            }).done(function(result, textStatus, jqXHR ) {
                console.log(result.data.usuario.nombre+" "+result.data.usuario.permiso+" "+result.data.usuario.concesionario);

                if(result.success){


                    $.ajax({

                        data: {"usuario":result.data.usuario, "token":result.token},

                        type: "post",

                        // dataType: "json",

                        url:"<?php echo base_url('Sign/post_login') ?>",

                    })
                        .done(function( result, textStatus, jqXHR ) {
                            console.log(result);
                            if(result){
                                console.log("si");
                                window.location.replace("https://smartkitchen.cl/paywash/inicio/home");
                            }else{
                                swal("Error", "error en servidor , favor de reintentars", "error");
                            }


                        })
                        .fail(function( jqXHR, textStatus, errorThrown ) {
                            if ( console && console.log ) {
                                console.log( "La solicitud a fallado: " +  textStatus);
                            }
                            swal("Error", "La petición ha fallado", "error");
                            $("#loading3").find('img').removeClass('loading-on');
                            $("#loading3").find('img').addClass('loading-off');
                        });

                }else{
                    swal("Error", "Credenciales erroneas", "error");
                }
            }).fail((err)=>{
                swal("Error", "Credenciales erroneas", "error");
                console.log('Error login');
                console.log(err);
            });
        });

    });
</script>

</body>
</html>

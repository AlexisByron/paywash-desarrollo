@{
ViewBag.Title = "Detailed";
}
1.10.9
<div style="width:95%; margin:0 auto;">
    <table id="DetailedTable">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Mid</th>
            <th>Last Name</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        </thead>
    </table>
</div>
<style>
    tr.even {
        background-color: #F5F5F5 !important;
    }
</style>
@* Load datatable css *@
<link href="~/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="~/css/jquery.dataTables.css" rel="stylesheet" />
<link href="~/css/dataTables.tableTools.css" rel="stylesheet" />
@* Load datatable js *@
@section Scripts{
<script src="~/Scripts/jquery.dataTables.min.js"></script>
<script src="~/Scripts/jquery.js"></script>
<script src="~/Scripts/jquery.dataTables.js"></script>
<script src="~/Scripts/dataTables.tableTools.js"></script>

<script>
    $(document).ready(function () {
        var Table = $('#DetailedTable').DataTable({
            dom: 'T<"clear">lfrtip',
            tableTools: {
                "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
                "sRowSelect": "os",
                "sRowSelector": 'td:first-child',
                "aButtons": [
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "Detailed Report"
                    },
                    {
                        "sExtends": "print",
                        "bShowAll": false
                    }

                ]
            },
            "scrollX": true,
            "serverSide": true,
            "ajax": {
                "url": "GetDetailed",
                "type": "POST",
                "datatype": "json"
            },
            "bLengthChange": false,
            "searchHighlight": true,
            "processing": true,
            "autoWidth": false,
            "responsive": true,
            "bSort": true,
            "bInfo": false,
            "bFilter": true,
            "pageLength": 10,

            "columns" : [
                { "data": "Report.FirstName", "autoWidth": true,
                    "searchable": true,
                    "sortable": true
                },
                {
                    "data": "Report.MiddleName",
                    "searchable": false,
                    "sortable": false,
                    "autoWidth": true
                },
                {
                    "data": "Report.LastName", "autoWidth": true,
                    "searchable": true,
                    "sortable": true,
                },
                {
                    "data": "Report.StartDate",
                    "autoWidth": true,
                    "searchable": false,
                    "sortable": true,
                    "render": function (startDate) {
                        if (!startDate) {
                            return "NA";
                        }
                        else {
                            var date = new Date(parseInt(startDate.substr(6)));
                            var month = date.getMonth() + 1;
                            return month + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    }
                },
                {
                    "data": "Report.EndDate",
                    "autoWidth": true,
                    "searchable": false,
                    "sortable": true,
                    "render": function (endDate) {
                        if (!endDate) {
                            return "NA";
                        }
                        else {
                            var date = new Date(parseInt(endDate.substr(6)));
                            var month = date.getMonth() + 1;
                            return month + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    }
                }
            ]
        });

        $('#DetailedTable_filter input').unbind();
        $('#DetailedTable_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                Table.fnFilter($(this).val());
            }
        });

    });
</script>
}